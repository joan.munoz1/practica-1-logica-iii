package todo;

public class matrizEnTripletas {
    private tripleta[] V;


    public matrizEnTripletas(tripleta t) {
        int m = t.retornaFila();
        int n = t.retornaColumna();
        int p = (m*n)+2;
        int i;
        V = new tripleta[p];
        V[0] = t;
        for(i =1; i <= p-1; i++){
            V[i] = null;
        }
    }

    public int retornanumeroDeFilas(){
        tripleta t = V[0];
        return t.retornaFila();
    }

    public int retornanumeroDeColumnas(){
        tripleta t = V[0];
        return t.retornaColumna();
    }

    public int retornanumeroDeElementos(){
        tripleta t = V[0];
        return (int) t.retornaValor();
    }

    public tripleta retornaTripleta(int i){
        return this.V[i];
    }

    public void asignaTripleta(tripleta tx, int i){
        this.V[i] = tx;
    }
    public void asignaNumeroDeElementos(int n){
        tripleta t = V[0];
        t.asignaValor(n);
        V[0] = t;
    }

    public void muestraMatrizEnTripletas(){
        int i =1;
        tripleta t = retornaTripleta(0);
        int datos = (int) t.retornaValor();
        while(i <= datos){
            if(V[i] == null){

            } else {
                System.out.println(V[i].retornaFila() + "," + V[i].retornaColumna() + "," + (Integer) V[i].retornaValor());
            }
            i++;
        }
    }

    public void muestraMatrizEnCuadricula(){

        int i =1;
        tripleta t = retornaTripleta(0);
        int datos = (int) t.retornaValor();
        int j =1;
        while(i <= datos){
            if(V[i].retornaFila() != j){
                System.out.println();
                j = j+1;
            }
            if(V[i] == null){
            } else if(i == 1){
                System.out.print( "| " +  V[i].retornaValor() + " |");
            } else if(i < datos){
                System.out.print( "| " +  V[i].retornaValor()  + " |");
            } else {
                System.out.print( "| " +  V[i].retornaValor() + " |");
            }
            i++;
        }
    }

    public void insertaTripleta(tripleta ti){
        int i,j,p;
        tripleta t, tx;
        tx = retornaTripleta(0);
        p = (int) tx.retornaValor();
        i =1;
        t = retornaTripleta(i);
        while ((i<=p) && (t.retornaFila()< ti.retornaFila())){
            i = i+1;
            t = retornaTripleta(i);
        }
        while((i<=p) && (t.retornaFila() == ti.retornaFila()) && (t.retornaColumna() < ti.retornaColumna())){
            i = i+1;
            t = retornaTripleta(i);
        }
        p = p+1;
        j = p-1;
        while(j>=i){
            V[j+1] = V[j];
            j = j-1;
        }
        V[i] = ti;
        asignaNumeroDeElementos(p);
    }

    public matrizEnTripletas multiplicar(matrizEnTripletas b){
        int m,n,na,p,nb,i,j,k,filaActual,columnaActual,inicioFilaActual,suma;
        tripleta ti, tj, tx;
        matrizEnTripletas bt,c;
        m = retornanumeroDeFilas();
        n = retornanumeroDeColumnas();
        na = retornanumeroDeElementos();
        if(n != b.retornanumeroDeFilas()){
            System.out.println("No se pueden multiplicar las matrices");
            return null;
        }
        p = b.retornanumeroDeColumnas();
        nb = b.retornanumeroDeElementos();
        tx = new tripleta(m+1,0,0);
        asignaTripleta(tx,na+1);
        tx = new tripleta(m,p,0);
        c = new matrizEnTripletas(tx);
        bt = b.traspuesta();
        tx = new tripleta(p+1,0,0);
        bt.asignaTripleta(tx,nb+1);
        i = 1;
        ti = retornaTripleta(i);
        filaActual = ti.retornaFila();
        inicioFilaActual = i;
        k = 0;
        suma = 0;
        while(i <= na){
            j = 1;
            tj = bt.retornaTripleta(j);
            columnaActual = tj.retornaColumna();
            while(j <= nb+1){
                tj = bt.retornaTripleta(j);
                if(ti.retornaFila() != filaActual){
                    if(suma != 0){
                        k = k+1;
                        tx = new tripleta(filaActual, columnaActual, suma);
                        c.asignaTripleta(tx,k);
                        suma = 0;
                    }
                    while(tj.retornaFila() == columnaActual){
                        j = j+1;
                        tj = bt.retornaTripleta(j);
                    }
                    columnaActual = tj.retornaFila();
                    i = inicioFilaActual;
                    ti = retornaTripleta(i);
                }
                if(tj.retornaFila() != columnaActual){
                    if(suma != 0){
                        k = k+1;
                        tx = new tripleta(filaActual, columnaActual, suma);
                        c.asignaTripleta(tx,k);
                        suma = 0;
                    }
                    columnaActual = tj.retornaFila();
                    i = inicioFilaActual;
                    ti = retornaTripleta(i);
                }
                if(ti.retornaColumna() < tj.retornaColumna()){
                    i = i+1;
                    ti = retornaTripleta(i);
                }
                if(ti.retornaColumna() == tj.retornaColumna()){
                    int sti = (int)ti.retornaValor();
                    int stj = (int)tj.retornaValor();
                    suma = suma + sti * stj;
                    i = i+1;
                    j = j+1;
                    ti = retornaTripleta(i);
                    tj = bt.retornaTripleta(j);
                }
                j = j+1;
            }
            while(ti.retornaFila() == filaActual){
                i = i+1;
                ti = retornaTripleta(i);
            }
            inicioFilaActual = i;
            filaActual = ti.retornaFila();
        }
        c.asignaNumeroDeElementos(k);
        return c;
    }

    public matrizEnTripletas suma(matrizEnTripletas b){
        int ma, na, mb, nb, p, q, i, j, k, ss, fi, fj, ci, cj, vi,vj;
        tripleta ti, tj, tx;
        ma = retornanumeroDeFilas();
        na = retornanumeroDeColumnas();
        mb = b.retornanumeroDeFilas();
        nb = b.retornanumeroDeColumnas();
        p = retornanumeroDeElementos();
        q = b.retornanumeroDeElementos();
        if((ma != mb) || (na != nb)){
            System.out.println("Matrices de diferentes dimensiones, no se pueden sumar");
            return null;
        }
        ti = new tripleta(ma, na, 0);
        matrizEnTripletas c = new matrizEnTripletas(ti);
        i = 1;
        j = 1;
        k = 0;
        while((i <= p) && (j<= q)){
            ti = retornaTripleta(i);
            tj = b.retornaTripleta(j);
            fi = ti.retornaFila();

            fj = tj.retornaFila();
            k = k+1;
            if(fi < fj){
                c.asignaTripleta(ti, k);
                i = i+1;
            } else if(fi> fj){
                c.asignaTripleta(tj, k);
                j = j+1;
            } else if( fi == fj){
                ci = ti.retornaColumna();
                cj = tj.retornaColumna();
                if(ci < cj){
                    c.asignaTripleta(ti, k);
                    i = i+1;
                } else if(ci > cj){
                    c.asignaTripleta(tj, k);
                    j = j+1;
                } else if(ci == cj){
                    vi = (int) ti.retornaValor();
                    vj = (int) tj.retornaValor();
                    ss = vi+vj;
                    if(ss !=0){
                        tx = new tripleta(fi,ci,ss);
                        c.asignaTripleta(tx,k);
                    } else {
                        k = k-1;
                    }
                    i = i+1;
                    j = j+1;
                }
            }
        }
        while(i<= p){
            ti = retornaTripleta(i);
            k = k+1;
            c.asignaTripleta(ti,k);
            i = i+1;
        }
        while(j <= q){
            tj = b.retornaTripleta(j);
            k = k+1;
            c.asignaTripleta(tj,k);
            j = j+1;
        }
        c.asignaNumeroDeElementos(k);
        return c;
    }


    public matrizEnTripletas traspuesta(){
        int m,n,p,i,j,s[], t[];
        tripleta ti, tx;
        m = retornanumeroDeFilas();
        n = retornanumeroDeColumnas();
        p = retornanumeroDeElementos();
        ti = new tripleta(n,m,p);
        matrizEnTripletas b = new matrizEnTripletas(ti);
        s = new int[n+1];
        t = new int[n+1];
        for(i = 1; i<=n; i++){
            s[i] = 0;
        }
        for(i = 1; i<=p; i++){
            ti = retornaTripleta(i);
            s[ti.retornaColumna()] = s[ti.retornaColumna()]+1;
        }
        t[1] = 1;
        for(i = 2; i <=n; i++){
            t[i] = t[i-1] + s[i-1];
        }
        for(i = 1; i<=p;i++){
            ti = retornaTripleta(i);
            j = ti.retornaColumna();
            tx = new tripleta(j, ti.retornaFila(), ti.retornaValor());
            b.asignaTripleta(tx,t[j]);
            t[j] = t[j]+1;
        }
        return b;
    }


    public int[][] convertirEnMatrizClasica(){
        int m,n,p;
        tripleta t;
        int[][] a;
        m = retornanumeroDeFilas();
        n = retornanumeroDeColumnas();
        p = retornanumeroDeElementos();
        a = new int[m][n];
        for(int i = 1; i <= p; i++){
            t = retornaTripleta(i);
            a[t.retornaFila()][t.retornaColumna()] = (int)t.retornaValor();
        }
        return a;
    }

    public int retornaValor(int f, int c){
        int i =1;
        tripleta t = retornaTripleta(0);
        int datos = (int) t.retornaValor();
        while(i <= datos){
            if(V[i] == null){
            } else {
                if(f == V[i].retornaFila() && c == V[i].retornaColumna()){
                    return (Integer)V[i].retornaValor();
                } else {

                }
            }
            i++;
        }
        return 0;
    }

    public void asignaValorEnTripletaEnFilaColumna(int f, int c, int valor){
        int i =1;
        tripleta t = retornaTripleta(0);
        int datos = (int) t.retornaValor();
        while(i <= datos){
            if(V[i] == null){
            } else {
                if(f == V[i].retornaFila() && c == V[i].retornaColumna()){
                    V[i].asignaValor(valor);
                }
            }
            i++;
        }
    }

    public void asignaValor(int c, int f, int valor){
        int i =1;
        tripleta t = retornaTripleta(0);
        int datos = (int) t.retornaValor();
        while(i <= datos){
            if(V[i] == null){

            } else {
                t = retornaTripleta(i);
                if(t.retornaFila() == f && t.retornaColumna() == c){
                    t.asignaValor(valor);
                }
            }
            i++;
        }
    }
}
