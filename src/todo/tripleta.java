package todo;

public class tripleta {
    private int fila;
    private int columna;
    private Object valor;

    public tripleta(int f, int c, Object v){
        this.fila = f;
        this.columna = c;
        this.valor = v;
    }

    public void asignaFila(int f){
        this.fila = f;
    }

    public void asignaColumna(int c){
        this.columna = c;
    }

    public void asignaValor(Object v){
        this.valor = v;
    }

    public int retornaFila(){
        return this.fila;
    }

    public int retornaColumna(){
        return this.columna;
    }

    public Object retornaValor(){
        return this.valor;
    }
}
