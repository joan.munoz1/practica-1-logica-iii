package todo;

import java.util.Random;
import java.util.Scanner;

public class controlador {

    private int tamanoColumna;

    public int[][] crearMatriz(){
        Random rnd = new Random();
        int ale = rnd.nextInt(8)+1;
        int ato = rnd.nextInt(8)+1;
        if(ale <= 0){
            ale = 1;
        }
        if(ato <= 0){
            ato = 1;
        }
        int [][] matriz = new int[ale][ato];
        tamanoColumna = ato;
        llenarMatrizAleatoria(matriz, ale, ato);
        return matriz;
    }

    public int retornaColumna(){
        return tamanoColumna;
    }

    public void llenarMatrizAleatoria(int[][] matriz, int m, int n){
        Random rnd = new Random();
        int rango = 0;
        while(rango < m*n){
            int alea = rnd.nextInt(100);
            int fila = rnd.nextInt(m);
            int columna = rnd.nextInt(n);
            matriz[fila][columna] = alea;
            rango++;
        }
    }

    public int[][] crearMatrizQueSePuedaMultiplicar(){
        Random rnd = new Random();
        int columna = rnd.nextInt(8) +1;
        int [][] matriz = new int[tamanoColumna][columna];
        llenarMatrizAleatoria(matriz, tamanoColumna, columna);
        return matriz;
    }


    public void mostarMatrices(int[][] matriz){
        for(int i=0; i< matriz.length; i++){
            for(int j=0; j < matriz[i].length; j++){
                int w = i+1;
                int z = j+1;
                if(i == 0 && j == 0){
                    System.out.print( "[" + w + "," + z + "," + matriz[i][j] + " |");
                } else if(j == matriz[i].length - 1 && i == matriz.length-1){
                    System.out.print( w + "," + z + "," + matriz[i][j] + " ]");
                } else {
                    System.out.print( "|" + w + "," + z + "," + matriz[i][j] + " |");
                }
            }
            System.out.println();
        }
    }

    public matrizEnTripletas convertirNormalEnTripleta (int[][] matriz){
        tripleta t = new tripleta(matriz.length , matriz[0].length ,0);
        matrizEnTripletas ma = new matrizEnTripletas(t);
        int k=0;
        for(int i=0; i< matriz.length; i++){
            for(int j=0; j< matriz[0].length;j++){
                int w = i+1;
                int z = j+1;
                tripleta ta = new tripleta(w,z,matriz[i][j]);
                ma.insertaTripleta(ta);
                k=k+1;
            }
        }
        ma.asignaNumeroDeElementos(k);
        return ma;
    }
}

