package todo;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import javax.swing.*;


public class juego {
    
    //atributos
    JFrame ventana; 
    JPanel panelPresentacion; 
    JLabel fondoPresentacion; 
    JTextField numMinas; 
    JTextField numF; 
    JTextField numC;
    JButton jugar;
    JButton facil;
    JButton intermedio;
    JButton experto;
    JButton personalizado;
    JLabel tamanoFila; 
    JLabel tamanoColumna; 
    JLabel numeroMinas; 
    int filas; 
    int columnas; 
    int minas; 
    

    matrizEnTripletas mats;
    matrizEnTripletas auxmats;



    JPanel panelJuego;
    JLabel fondoJuego;
    JLabel marcadorTiempo;
    JLabel marcadorBanderas;
    JLabel matriz[][];
    Timer tiempo;
    int mat[][];
    int auxmat[][];
    Random aleatorio;
    int min;
    int seg;
    int contBanderas;
    int contRestante;
    
    
    //constructor
    public juego(){
        
        //ventana
        ventana = new JFrame("Buscaminas UwU"); 
        ventana.setSize(650, 500);
        ventana.setLocationRelativeTo(null);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setResizable(false); 
        ventana.setLayout(null); 
        
        //Jpanel presentacion
        panelPresentacion = new JPanel(); 
        panelPresentacion.setSize(ventana.getWidth(), ventana.getHeight());
        panelPresentacion.setLocation(0,0);
        panelPresentacion.setLayout(null); 
        panelPresentacion.setVisible(true);
        
        // Imagen de presentacion
        fondoPresentacion = new JLabel(); 
        fondoPresentacion.setIcon(new ImageIcon("imagenes/fondopresentacion.jpg"));
        fondoPresentacion.setBounds(0, 0, panelPresentacion.getWidth(), panelPresentacion.getHeight());
        fondoPresentacion.setVisible(true); 
        panelPresentacion.add(fondoPresentacion,0); 
        
        
        // Label numero de filas  
        tamanoFila = new JLabel("Ingrese Numero de Filas"); 
        tamanoFila.setSize(200, 30);
        tamanoFila.setLocation(panelPresentacion.getWidth() - tamanoFila.getWidth() - 100, 25);
        tamanoFila.setVisible(true); 
        panelPresentacion.add(tamanoFila, 0); 
        
        
        // caja de texto numero de filas 
        numF = new JTextField(""); 
        numF.setSize(200, 30);
        numF.setLocation(panelPresentacion.getWidth() - numF.getWidth() - 100, 50);
        numF.setVisible(true); 
        panelPresentacion.add(numF, 0); 
        
        // Label numero de columnas  
        tamanoColumna = new JLabel("Ingrese Numero de Columnas"); 
        tamanoColumna.setSize(200, 30);
        tamanoColumna.setLocation(panelPresentacion.getWidth() - tamanoColumna.getWidth() - 100, 75);
        tamanoColumna.setVisible(true); 
        panelPresentacion.add(tamanoColumna, 0); 
        
        
        // caja de texto numero de columnas 
        numC = new JTextField(""); 
        numC.setSize(200, 30);
        numC.setLocation(panelPresentacion.getWidth() - numC.getWidth() - 100, 100);
        numC.setVisible(true); 
        panelPresentacion.add(numC, 0); 
        
        
        // Label numero de minas
        numeroMinas = new JLabel("Ingrese numero de minas"); 
        numeroMinas.setSize(200, 30);
        numeroMinas.setLocation(panelPresentacion.getWidth() - numeroMinas.getWidth() - 100, 125);
        numeroMinas.setVisible(true); 
        panelPresentacion.add(numeroMinas, 0); 
        
        // caja de texto numero de minas
        numMinas = new JTextField(""); 
        numMinas.setSize(200, 30);
        numMinas.setLocation(panelPresentacion.getWidth() - numF.getWidth() - 100, 150);
        numMinas.setVisible(true); 
        panelPresentacion.add(numMinas, 0); 
        
        // boton de jugar
        jugar = new JButton("JUGAR"); 
        jugar.setSize(200, 30);
        jugar.setLocation(panelPresentacion.getWidth() - numF.getWidth() - 100, 200); 
        ventana.setVisible(true); 
        jugar.setBackground(Color.RED);
        panelPresentacion.add(jugar, 0);

        // boton de facil
        facil = new JButton("FACIL");
        facil.setSize(150, 30);
        facil.setLocation(60, 50);
        ventana.setVisible(true);
        facil.setBackground(Color.GREEN);
        panelPresentacion.add(facil, 0);

        // boton de intermedio
        intermedio = new JButton("INTERMEDIO");
        intermedio.setSize(150, 30);
        intermedio.setLocation(60, 100);
        ventana.setVisible(true);
        intermedio.setBackground(Color.YELLOW);
        panelPresentacion.add(intermedio, 0);


        // boton de experto
        experto = new JButton("EXPERTO");
        experto.setSize(150, 30);
        experto.setLocation(60, 150);
        ventana.setVisible(true);
        experto.setBackground(Color.RED);
        panelPresentacion.add(experto, 0);

        // boton de Personalizado
        personalizado = new JButton("PERSONALIZADO");
        personalizado.setSize(150, 30);
        personalizado.setLocation(60, 200);
        ventana.setVisible(true);
        personalizado.setBackground(Color.CYAN);
        panelPresentacion.add(personalizado, 0);

        //Jpanel juego

        panelJuego = new JPanel();
        panelJuego.setSize(ventana.getWidth(),ventana.getHeight());
        panelJuego.setLocation(0,0);
        panelJuego.setLayout(null);
        panelJuego.setVisible(true);

        //Listeners de los botones de las dificultades
        facil.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                numF.setText("8");
                numC.setText("8");
                numMinas.setText("10");
                super.mousePressed(e);
            }
        });

        intermedio.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                numF.setText("16");
                numC.setText("16");
                numMinas.setText("40");
                super.mousePressed(e);
            }
        });

        experto.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                numF.setText("16");
                numC.setText("30");
                numMinas.setText("99");
                super.mousePressed(e);
            }
        });

        personalizado.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                numF.setText("");
                numC.setText("");
                numMinas.setText("");
                super.mousePressed(e);
            }
        });

        //Imagen de la pantalla de juego
        fondoJuego = new JLabel();
        fondoJuego.setIcon(new ImageIcon("imagenes/fondoJuego.jpg"));
        fondoJuego.setBounds(0, 0, panelJuego.getWidth(), panelJuego.getHeight());
        fondoJuego.setVisible(true);
        panelJuego.add(fondoJuego,0);

        aleatorio = new Random();

        // Configuracion del temporizador
        min = 0;
        seg = 0;
        marcadorTiempo = new JLabel("tiempo: "+min+":"+seg);
        marcadorTiempo.setSize(70, 30);
        marcadorTiempo.setVisible(true);
        marcadorTiempo.setForeground(Color.white);
        panelJuego.add(marcadorTiempo,0);

        tiempo = new Timer (1000, new ActionListener ()
        {
            public void actionPerformed(ActionEvent e)
            {
                
                seg++;
                if (seg == 60){
                    seg = 0;
                    min++;
                    
                }
            marcadorTiempo.setText("tiempo: "+min+":"+seg);
        }});
        
        
        
        marcadorBanderas = new JLabel("Banderas: "+contBanderas);
        marcadorBanderas.setSize(100, 30);
        marcadorBanderas.setVisible(true);
        marcadorBanderas.setForeground(Color.white);
        panelJuego.add(marcadorBanderas,0);
        
        //Evento de darle click al boton jugar

        jugar.addMouseListener(new MouseAdapter(){
            @Override
            public void mousePressed(MouseEvent e){
                
                // Validacion de los campos
                if(numF.getText().isEmpty()){
                    JOptionPane.showMessageDialog(ventana, "Las filas no pueden ser vacias");
                    numF.requestFocus();
                }
                try{
                    Integer.parseInt(numF.getText());
                } catch (NumberFormatException numberFormatException){
                    JOptionPane.showMessageDialog(ventana, "Las filas deben ser un numero");
                    numF.requestFocus();
                    return;
                }

                if(numC.getText().isEmpty()){
                    JOptionPane.showMessageDialog(ventana, "Las columnas no pueden ser vacias");
                    numC.requestFocus();
                }
                try{
                    Integer.parseInt(numC.getText());
                } catch (NumberFormatException numberFormatException){
                    JOptionPane.showMessageDialog(ventana, "Las filas deben ser un numero");
                    numC.requestFocus();
                    return;
                }

                if(numMinas.getText().isEmpty()){
                    JOptionPane.showMessageDialog(ventana, "El numero minas no pueden ser vacio");
                    numMinas.requestFocus();
                }
                try{
                    Integer.parseInt(numMinas.getText());
                } catch (NumberFormatException numberFormatException){
                    JOptionPane.showMessageDialog(ventana, "El numero de minas debe ser un numero");
                    numMinas.requestFocus();
                    return;
                }

                System.out.println("presione jugar");
                filas = Integer.parseInt(numF.getText()); 
                columnas = Integer.parseInt(numC.getText());  
                minas = Integer.parseInt(numMinas.getText());

                
                //cambiando de paneles
                panelPresentacion.setVisible(false);
                panelJuego.setVisible(true);
                ventana.add(panelJuego);

                mat = new int[filas][columnas];
                auxmat = new int [filas][columnas];
                matriz = new JLabel[filas][columnas];
                tripleta t = new tripleta(filas, columnas, 0);
                tripleta tx = new tripleta(filas, columnas, 0);
                mats = new matrizEnTripletas(t);
                auxmats = new matrizEnTripletas(tx);
                for (int i = 0; i < filas; i++) {
                    for (int j = 0; j < columnas; j++) {
                        matriz[i][j] = new JLabel();
                    }

                }
                tiempo.start();
                contBanderas = minas;
                marcadorBanderas.setText("Banderas: "+contBanderas);
                
                //Se agrega la maatriz de imagenes
                inicializarMatriz();
                
                for (int i = 0; i < filas; i++) {
                    for (int j = 0; j < columnas; j++) {
                        //Evento del mouse para cada imagen de la matriz
                        matriz[i][j].addMouseListener(new MouseAdapter(){
                        @Override
                        public void mousePressed(MouseEvent e){
                     
                            for (int k = 0; k < filas; k++) {
                                for (int l = 0; l < columnas; l++) {
                                    if (e.getSource() == matriz[k][l]) {
                                        if(e.getButton() == MouseEvent.BUTTON1){    
                                            System.out.println(k+" "+l);

                                            //Condiciones para cuando se escoge un cuadrito del tablero

                                            if (mat[k][l] != -2 && mat[k][l] != 0 && auxmat[k][l] != -3){
                                                auxmats.asignaValor(k,l,mats.retornaValor(k,l));
                                                auxmat[k][l] = mat[k][l];
                                                matriz[k][l].setIcon(new ImageIcon("imagenes/"+auxmat[k][l]+".png"));
                                            }
                                            //Cuando se escoge una mina
                                            if (mat[k][l] == -2 && auxmat[k][l] != -3) {
                                                //Se muestran las demas minas
                                                for (int m = 0; m < filas; m++) {
                                                    for (int n = 0; n < columnas; n++) {
                                                        if (mat[m][n] == -2){
                                                            auxmats.asignaValor(m,n,mats.retornaValor(m,n));
                                                            auxmat[m][n] = mat[m][n];
                                                            matriz[m][n].setIcon(new ImageIcon("imagenes/"+auxmat[m][n]+".png"));
                                                        }
                                                    }

                                                }
                                                JOptionPane.showMessageDialog(ventana, "BOOM PERDISTE");
                                                // Reiniciar
                                                for (int m = 0; m < filas; m++) 
                                                    for (int n = 0; n < columnas; n++)
                                                        matriz[m][n].setVisible(false);
                                                seg = 0;
                                                min = 0;
                                                contBanderas = minas;
                                                numVecinos();
                                                ventana.setSize(650, 500);
                                                panelPresentacion.setSize(ventana.getWidth(), ventana.getHeight());
                                                fondoPresentacion.setSize(ventana.getWidth(), ventana.getHeight());
                                                panelJuego.setVisible(false);
                                                panelPresentacion.setVisible(true);
                                                ventana.add(panelPresentacion);
                                                numF.setText("");
                                                numC.setText("");
                                                numMinas.setText("");

                                            }
                                            //Cuando se escoge un espacio en blanco
                                            if(mat[k][l] == 0 && auxmat[k][l] != -3) { // mats.retornaValor(k,l) == 0 || auxmats.retornaValor(k,l) == -3 ;

                                                recursiva(k,l);
                                                numVecinos();
                                            }
                                            // Se valida si el usuario gano
                                            contRestante = 0;
                                            for (int m = 0; m < filas; m++) {
                                                for (int n = 0; n < columnas; n++) {
                                                    if (auxmat[m][n] == -1 || auxmat[m][n] == -3) // auxmats.retornaValor(m,n) == -1 || auxmats.retornaValor(m,n) == -3 ;
                                                        contRestante++;
                                                }
                                                
                                            }
                                            
                                            if (contRestante == minas){
                                                for (int m = 0; m < filas; m++) 
                                                    for (int n = 0; n < columnas; n++)
                                                        matriz[m][n].setVisible(false);
                                                seg = 0;
                                                min = 0;

                                                //Se reinicia y se restablecen valores graficos y variables
                                                contBanderas = minas;
                                                numVecinos();
                                                ventana.setSize(650, 500);
                                                panelPresentacion.setSize(ventana.getWidth(), ventana.getHeight());
                                                fondoPresentacion.setSize(ventana.getWidth(), ventana.getHeight());
                                                panelJuego.setVisible(false);
                                                panelPresentacion.setVisible(true);
                                                ventana.add(panelPresentacion);
                                                numF.setText("");
                                                numC.setText("");
                                                numMinas.setText("");
                                                JOptionPane.showMessageDialog(ventana, "GANASTE");
                                                
                                            }

                                        }else if(e.getButton() == MouseEvent.BUTTON3){
                                            // Esta parte contiene lo relacionado con las banderas y su funcionalidad
                                            // Las banderas funcionan con click derecho
                                            if(auxmat[k][l] == -1 && contBanderas>0 ){
                                                auxmat[k][l] = -3;
                                                auxmats.asignaValor(k,l,-3);
                                                contBanderas --; 
                                                matriz[k][l].setIcon(new ImageIcon("imagenes/"+auxmat[k][l]+".png"));
                                                marcadorBanderas.setText("Banderas: " + contBanderas); 
                                            }
                                            else if(auxmat[k][l] == -3){
                                                auxmat[k][l] = -1;
                                                auxmats.asignaValor(k,l,-1);
                                                contBanderas++; 
                                                matriz[k][l].setIcon(new ImageIcon("imagenes/"+auxmat[k][l]+".png"));
                                                marcadorBanderas.setText("Banderas: " + contBanderas); 
                                            }
                                            
                                        }
                                        
                                    }
                                }
                            }
                        
                        }});
                    }
                    
                }
            }
        }); 
        // Se hace visible el panel de presentacion
        
        ventana.add(panelPresentacion); 
        ventana.setVisible(true); 
    }
    
    public void inicializarMatriz(){
        // Metodo en el cual se agrega-inicializa-crea la matriz de imagenes la cual es un JLabel
        // Con sus minas y valores correpondientes

	controlador controlador = new controlador();
	int f;
	int c;	
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++){
                mat[i][j] = 0;
                auxmat[i][j] = -1;
                
            }
        }
                           
	for (int i = 0; i < minas; i++){
		
		do{
			f = aleatorio.nextInt(filas);
			c = aleatorio.nextInt(columnas);
		}while (mat[f][c] == -2);
			mat[f][c] = -2;
	}

	// Se establecen los correctos con los valores adyacentes

	for (int i = 0; i < filas; i++) {
		for (int j = 0; j < columnas; j++) {
                    if(mat[i][j] == -2){
                        
                        // Arriba
                        if(i >0 && mat[i-1][j] != -2){
                            mat[i-1][j]++;
                        }
                        
                        // Abajo
                        if(i < filas-1 && mat[i+1][j] != -2){
                            mat[i+1][j]++;
                        }
                        
                        // Izquierda
                        if(j >0 && mat[i][j-1]!= -2){
                            mat[i][j-1]++;
                        }
                        
                        // Derecha
                        if(j <columnas-1 && mat[i][j+1] != -2){
                            mat[i][j+1]++;
                        }
                        //Esquina superior Izquierda
                        if(i > 0 && j>0 && mat[i-1][j-1] != -2){
                            mat[i-1][j-1]++;
                        }    
                        //Esquina inferior Izquierda
                        if(i<filas-1 && j>0 && mat[i+1][j-1] != -2){
                            mat[i+1][j-1]++;
                        }
                        //Esquina Superior Derecha
                        if(i > 0 && j<columnas-1 && mat[i-1][j+1] != -2){
                            mat[i-1][j+1]++;
                        }
                        //Esquina inferior Derecha
                        if(i<filas-1 && j<columnas-1 && mat[i+1][j+1] != -2){
                            mat[i+1][j+1]++;
                        }
                        
                    }
		
		}

	}
	    // Se transforma la matriz, comprobacion en consola de valores por impresion
        // Se ponen las imagenes a cada posicion del Label y parte grafica
        
        
        ventana.setSize(100+(columnas*25), 150+(filas*25));
        panelJuego.setSize(100+(columnas*25), 150+(filas*25));
        marcadorTiempo.setLocation(10, 15);
        marcadorBanderas.setLocation(10, 30);
        mats = controlador.convertirNormalEnTripleta(mat);
        mats.muestraMatrizEnCuadricula();

        int i =1;
        tripleta t = mats.retornaTripleta(0);
        int datos = (int) t.retornaValor();
        while(i <= datos){
            if(mats.retornaTripleta(i) == null){

            } else {
                System.out.println(mats.retornaTripleta(i).retornaFila() + "," + mats.retornaTripleta(i).retornaColumna() + "," + (Integer) mats.retornaTripleta(i).retornaValor());
            }
            i++;
        }
        for (i = 0; i < filas; i++) {
		for (int j = 0; j < columnas; j++) {
		    //Se asiganan las imagenes correspondientes en el Label matriz
            System.out.println(mats.retornaValor(i,j));
			matriz[i][j] =new JLabel();
                        matriz[i][j].setSize(25, 25);
                        matriz[i][j].setLocation(j, j);
                        matriz[i][j].setLocation(50+(j*25), 75+(i*25));
                        matriz[i][j].setIcon(new ImageIcon("imagenes/"+auxmat[i][j]+".png"));
                        matriz[i][j].setVisible(true);
                        panelJuego.add(matriz[i][j],0);
		}
		System.out.println("");

	}
    }

    public void recursiva(int i, int j){

        // Metodo encargado de "limpiar camino" cuando se da click en un 0 y restablecer las imagenes para que aparezcan
        // Dependiendo de que valor tenga la respectiva posicion

        controlador controlador = new controlador();
        auxmat[i][j] = mat[i][j];
        mat[i][j] = 9;
        
        //derecha
        if( j < columnas - 1 && mat[i][j+1] == 0 && auxmat[i][j+1] != -3){
            recursiva(i, j+1);
        }else if (j < columnas - 1 && mat[i][j+1] != 0 && mat[i][j+1] != -2 && mat[i][j+1] != 9 && auxmat[i][j+1] != -3)
            auxmat[i][j+1] = mat[i][j+1];
        
        //izquierda
        if( j > 0 && mat[i][j-1] == 0 && auxmat[i][j-1] != -3){
            recursiva(i, j-1);
        }else if (j > 0 && mat[i][j-1] != 0 && mat[i][j-1] != -2 && mat[i][j-1] != 9 && auxmat[i][j-1] != -3)
            auxmat[i][j-1] = mat[i][j-1];
        
        //arriba
        if( i > 0 && mat[i-1][j] == 0 && auxmat[i-1][j] != -3){
            recursiva(i-1, j);
        }else if ( i > 0 && mat[i-1][j] != 0 && mat[i-1][j] != -2 && mat[i-1][j] != 9 && auxmat[i-1][j] != -3)
            auxmat[i-1][j] = mat[i-1][j];
        
        //abajo
        if( i < filas - 1 && mat[i+1][j] == 0 && auxmat[i+1][j] != -3){
            recursiva(i+1, j);
        }else if (i < filas - 1 && mat[i+1][j] != 0 && mat[i+1][j] != -2 && mat[i+1][j] != 9 && auxmat[i+1][j] != -3)
            auxmat[i+1][j] = mat[i+1][j];
        
        
        matriz[i][j].setIcon(new ImageIcon("imagenes/"+auxmat[i][j]+".png"));
        
    }

    public void numVecinos() {
        //Se realiza la operacion de actualizacion de los vecinos respecto a la matriz de imagenes
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                System.out.print(auxmat[i][j]+" ");
            matriz[i][j].setIcon(new ImageIcon("imagenes/"+auxmat[i][j]+".png")); 
            }
            System.out.println("");
        }
    
    
    }


}